/*
 * Author: Tate Anderson
 * Date: 3/4/20
 * Filename: motors.h
 * Description: This is the header file for the motors functions
 */

#ifndef MOTORS_H_
#define MOTORS_H_
#include <Arduino.h>
#endif

void waitLong();
void waitShort();
void waitSuperShort();
void motorSetup();
void leftForward();
void leftReverse();
void rightForward();
void rightReverse();
void motorTest();
void forward();
void off();
void reverse();
void slow();
void fast();