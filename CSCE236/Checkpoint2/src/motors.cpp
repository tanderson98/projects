/*
 * Author: Tate Anderson
 * Date: 3/4/20
 * Filename: motors.cpp
 * Description: This is the implementation file for the motors functions
 */

#include "motors.h"
#include <Arduino.h>


void waitLong()
{
  uint32_t i = 0xffffff;
  for(i = 0; i < 0x3fffff; i++)
  {
    asm volatile("nop");
  }
}

void waitShort()
{
  uint32_t i = 0xfffff;
  for(i = 0; i < 0xfffff; i++)
  {
    asm volatile("nop");
  }
}

void waitSuperShort()
{
  uint32_t i = 0xffffff;
  for(i = 0; i < 0xf0000; i++)
  {
    asm volatile("nop");
  }
}

void motorSetup()
{
    pinMode(A0, OUTPUT);
    pinMode(A1, OUTPUT);
    pinMode(A2, OUTPUT);
    pinMode(A3, OUTPUT);

    pinMode(5, OUTPUT);
    pinMode(6, OUTPUT);

    TCCR0A = 1 << (COM0A1) | 0 << (COM0A0) | 1 << (COM0B1) | 0 << (COM0B0) | 1 << (WGM00) | 1 << (WGM01);
    TCCR0B = 0 << (WGM02) | 1 << (CS02)| 0 << (CS01)| 0 << (CS00);

    OCR0A = 85;
    OCR0B = 75;
}

void leftForward()
{
  digitalWrite(A3, HIGH);
  digitalWrite(A2, LOW);
}

void leftReverse()
{
  digitalWrite(A2, HIGH);
  digitalWrite(A3, LOW);
}

void rightForward()
{
  digitalWrite(A1, HIGH);
  digitalWrite(A0, LOW);
}

void rightReverse()
{
  digitalWrite(A0, HIGH);
  digitalWrite(A1, LOW);
}

void off()
{
  digitalWrite(A0, LOW);
  digitalWrite(A1, LOW);
  digitalWrite(A2, LOW);
  digitalWrite(A3, LOW);
}

void forward()
{
  digitalWrite(A1, HIGH);
  digitalWrite(A3, HIGH);
}

void reverse()
{
  rightReverse();
  leftReverse();
}

void motorTest()
{
    //forward
    leftForward();
    rightForward();
    waitLong();
    off();
    waitLong();

    //backward
    leftReverse();
    rightReverse();
    waitLong();
    off();
    waitLong();

    //right/left < 45
    rightReverse();
    leftForward();  
    waitShort();
    off();
    waitLong();  

    leftReverse();
    rightForward();  
    waitShort();
    off();
    waitLong();  

    //right/left > 45
    rightReverse();
    leftForward();  
    waitShort();
    waitShort();
    off();
    waitLong();  

    leftReverse();
    rightForward();  
    waitShort();
    waitShort();
    off();
    waitLong();
}

void slow() {
  OCR0A = 55;
  OCR0B = 55;
}

void fast() {
  OCR0A = 85;
  OCR0B = 85;
}