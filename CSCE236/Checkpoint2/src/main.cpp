/*--------------------------------------------------------------------
Name:   Tate Anderson
Date:   16 April 20  
Course: CSCE 236 Embedded Systems (Spring 2020) 
File:   main.cpp
HW/Lab: Lab 5/Project 2, Decoding an IR Packet

Purp: Uses counters and interrupts to decode an IR packet for a 
    remote. 

Doc:  <list the names of the people who you helped>
    <list the names of the people who assisted you>

Academic Integrity Statement: I certify that, while others may have 
assisted me in brain storming, debugging and validating this program, 
the program itself is my own work. I understand that submitting code 
which is the work of other individuals is a violation of the honor   
code.  I also understand that if I knowingly give my original work to 
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/
#include <avr/io.h>
#include <Arduino.h>
#include "motors.h"
#include "ultrasonic.h"

void setup() {
  Serial.begin(9600);
  Serial.print("Starting up...");
  servoSetup();
  motorSetup();
  ultrasonicSetup();
}

/*
 * main loop
 */
void loop() {
  followWall();
  waitShort();
  avoidObstacle();
  waitShort();
}