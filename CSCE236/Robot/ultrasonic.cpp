/*
 * Author: Tate Anderson
 * Date: 3/4/20
 * Filename: ultrasonic.cpp
 * Description: This is the implementation file for the ultrasonic functions
 */

#include "ultrasonic.h"
#include <Arduino.h>

void ledInit() {
  DDRB_Reg |= (1 << 1);
  DDRB_Reg |= (1 << 2);
}

bool ultrasonicRead() {
  digitalWrite(11, HIGH);
  delayMicroseconds(10);
  digitalWrite(11, LOW);
  
  //=====================================

  TCCR1B |= (1 << 6);			/* capture on rising edge */ 
	while ((TIFR1 & (1<<ICF1)) == 0);  /* monitor for capture*/
	t1 = ICR1;			
	TIFR1 = (1<<ICF1);		/* clear capture flag */

  TCCR1B &= ~(1 << 6);			/* capture on falling edge */ 
	while ((TIFR1 & (1<<ICF1)) == 0);  /* monitor for falling edge capture */
  t2 = ICR1;
  TIFR1 = (1<<ICF1);

  t = t2 - t1;

  distance = t / 0.25 / 148;
	Serial.print(distance);
  Serial.println(" inches");
  if( t < 300  && t > 0)
  {
    return 1;
  }
  else
  {
    return 0;
  }
  //=====================================
  
}

void rotateServo() {
  OCR2B = 3;
  delay(1000);
  for(int i = 0; i < 5; i++)
  {
    ultrasonicRead();
  }
  while(ultrasonicRead() == 1)
  {
    PORTB_Reg |= (1 << 2);
  }
  PORTB_Reg &= ~(1 << 2);

  OCR2B = 11;
  delay(1000);
  for(int i = 0; i < 5; i++)
  {
    ultrasonicRead();
  }
  while(ultrasonicRead() == 1)
  {
    PORTB_Reg |= (1 << 2);
    PORTB_Reg |= (1 << 1);
  }
  PORTB_Reg &= ~(1 << 2);
  PORTB_Reg &= ~(1 << 1);

  OCR2B = 19;
  delay(1000);
  for(int i = 0; i < 5; i++)
  {
    ultrasonicRead();
  }
  while(ultrasonicRead() == 1)
  {
    PORTB_Reg |= (1 << 1);
  }
  PORTB_Reg &= ~(1 << 1);

  OCR2B = 11;
  delay(1000);
  for(int i = 0; i < 5; i++)
  {
    ultrasonicRead();
  }
  while(ultrasonicRead() == 1)
  {
    PORTB_Reg |= (1 << 2);
    PORTB_Reg |= (1 << 1);
  }
  PORTB_Reg &= ~(1 << 2);
  PORTB_Reg &= ~(1 << 1);
}
