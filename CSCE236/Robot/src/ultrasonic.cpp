/*
 * Author: Tate Anderson
 * Date: 3/4/20
 * Filename: ultrasonic.cpp
 * Description: This is the implementation file for the ultrasonic functions
 */

#include "ultrasonic.h"
#include "motors.h"
#include <Arduino.h>

int t, t1, t2, distance;

void ultrasonicSetup() {
  
  //ULTRASONIC TIMER
  //============================================================
  pinMode(8, INPUT);
  pinMode(11, OUTPUT);
  //Init counter1
  TCCR1A |= (0 << WGM11) | (0 << WGM10); //Normal mode 0xffff top, rolls over
  TCCR1B |= (0 << WGM12) | (0 << CS12) | (1 << CS11) | (1 << CS10);
  TIMSK1 |= 1<<ICIE1;//enable input capture interrupt

  //Set counter to zero, high byte first
  TCNT1H = 0;
  TCNT1L = 0;  
  
  //Make sure interrupts are disabled 
  TIMSK1 = 0;
  TIFR1 = 0;

  TCCR1A = 0;
	TIFR1 = (1<<ICF1);		/* clear input capture flag */
}

void servoSetup() {
  //SERVO TIMER
  //============================================================
  pinMode(3, OUTPUT);
  //pinMode(11, OUTPUT);
  TCCR2A = 1 << (COM2A0) | 1 << (COM2B1) | 1 << (WGM20);
  TCCR2B = 1 << (WGM22) | 1 << (CS22)| 1 << (CS21)| 1 << (CS20);
  OCR2A = 156;
  OCR2B = 4;
}

void ledInit() {
  DDRB_Reg |= (1 << 1);
  DDRB_Reg |= (1 << 2);
}

int ultrasonicRead() {
  digitalWrite(11, HIGH);
  delayMicroseconds(10);
  digitalWrite(11, LOW);
  
  //=====================================

  TCCR1B |= (1 << 6);			/* capture on rising edge */ 
	while ((TIFR1 & (1<<ICF1)) == 0);  /* monitor for capture*/
	t1 = ICR1;			
	TIFR1 = (1<<ICF1);		/* clear capture flag */

  TCCR1B &= ~(1 << 6);			/* capture on falling edge */ 
	while ((TIFR1 & (1<<ICF1)) == 0);  /* monitor for falling edge capture */
  t2 = ICR1;
  TIFR1 = (1<<ICF1);

  t = t2 - t1;

  distance = t / 0.25 / 148;
	Serial.print(distance);
  Serial.println(" inches");
  return distance;
  // if( t < 300  && t > 0)
  // {
  //   return 1;
  // }
  // else
  // {
  //   return 0;
  // }
  //=====================================
  
}

void rotateServo() {
  OCR2B = 3;
  delay(1000);
  for(int i = 0; i < 5; i++)
  {
    ultrasonicRead();
  }
  while(ultrasonicRead() == 1)
  {
    PORTB_Reg |= (1 << 2);
  }
  PORTB_Reg &= ~(1 << 2);

  OCR2B = 11;
  delay(1000);
  for(int i = 0; i < 5; i++)
  {
    ultrasonicRead();
  }
  while(ultrasonicRead() == 1)
  {
    PORTB_Reg |= (1 << 2);
    PORTB_Reg |= (1 << 1);
  }
  PORTB_Reg &= ~(1 << 2);
  PORTB_Reg &= ~(1 << 1);

  OCR2B = 19;
  delay(1000);
  for(int i = 0; i < 5; i++)
  {
    ultrasonicRead();
  }
  while(ultrasonicRead() == 1)
  {
    PORTB_Reg |= (1 << 1);
  }
  PORTB_Reg &= ~(1 << 1);

  OCR2B = 11;
  delay(1000);
  for(int i = 0; i < 5; i++)
  {
    ultrasonicRead();
  }
  while(ultrasonicRead() == 1)
  {
    PORTB_Reg |= (1 << 2);
    PORTB_Reg |= (1 << 1);
  }
  PORTB_Reg &= ~(1 << 2);
  PORTB_Reg &= ~(1 << 1);
}

void followWall() {
  forward();
  waitShort();
  off();
  ultrasonicRead();
  if(ultrasonicRead() > 12)
  {
    OCR0B = 50;
    leftForward();
    waitSuperShort();
    off();
    OCR0B = 80;
  }
  else if(ultrasonicRead() < 8)
  {
    OCR0A = 60;
    rightForward();
    waitSuperShort();
    off();
    OCR0A = 90;
  }
}
