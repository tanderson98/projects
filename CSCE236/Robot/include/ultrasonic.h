/*
 * Author: Tate Anderson
 * Date: 3/4/20
 * Filename: ultrasonic.h
 * Description: This is the header file for the ultrasonic functions
 */

#ifndef ULTRASONIC_H_
#define ULTRASONIC_H_
#include <Arduino.h>
#endif

#define DDRB_Reg (*((volatile uint8_t *) 0x24))
#define PORTB_Reg (*((volatile uint8_t *) 0x25))

void ultrasonicSetup();
void servoSetup();
void ledInit();
int ultrasonicRead();
void rotateServo();
void followWall();