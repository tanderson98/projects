import mido
import numpy as np

notesAll = []
notesUnique = []
transitionMatrix = []

def main():

	global notesAll
	global notesUnique
	global transitionMatrix

	mid = mido.MidiFile('bach_bourree.mid')
	for i, track in enumerate(mid.tracks):
	    for msg in track:
	        if msg.type == 'note_on':
	        	notesAll.append(msg.note)
	        	if msg.note not in notesUnique:
	        		notesUnique.append(msg.note)

	# Zero transition matrix
	arr = [0] * len(notesUnique)
	transitionMatrix = np.zeros((len(notesUnique), len(notesUnique)))

	column = None
	row = None

	# Loop through all notes, add to transition matrix
	for i in range(len(notesAll)-1):
		for j in range(len(notesUnique)):
			if notesAll[i] == notesUnique[j]:
				column = j
			if notesAll[i+1] == notesUnique[j]:
				row = j
		transitionMatrix[row][column] += 1

	# sum columns, then divide each entry by the sum of the column that
	# it is in in order to make the sum of each column equal to 1
	for column in range(len(notesUnique)):
		columnSum = 0
		for row1 in range(len(notesUnique)):
			columnSum += transitionMatrix[row1][column]
		for row in range(len(notesUnique)):
			transitionMatrix[row][column] = transitionMatrix[row][column]/columnSum

	# create output .midi file
	midNew = mido.MidiFile(type=0)
	track = mido.MidiTrack()

	# TODO: choose first note, then write n notes to midi file, transitioning each time
	# for now, we will start on note 71 (first in array) and write 400 notes
	firstNote = 0
	noteIndexArray = new(firstNote)
	songLength = 200
	time = 96

	# write first note
	noteVal = noteValue(noteIndexArray)
	track.append(mido.Message('note_on', note=noteVal, velocity=64, time=time))
	track.append(mido.Message('note_off', note=noteVal, velocity=64, time=time))
	
	# choose notes
	for x in range(songLength):
		index = choose(noteIndexArray)
		noteIndexArray = new(index)
		noteVal = noteValue(noteIndexArray)
		track.append(mido.Message('note_on', note=noteVal, velocity=64, time=time))
		track.append(mido.Message('note_off', note=noteVal, velocity=64, time=time))
		x += 1

	# append track, and save
	midNew.tracks.append(track)
	midNew.save('new_song.mid')

#compute the transition matrix times the current note vector to find next room probability
def transition(current):
	global transitionMatrix
	product = transitionMatrix.dot(current)
	return product

#choose next note
def choose(current):
	probability = transition(current)
	index = []
	global notesUnique
	index = np.arange(len(notesUnique))
	choice = np.random.choice(a = index, p = probability)
	return choice

#create new note vector
def new(index):
	global notesUnique
	new = [None] * len(notesUnique)
	for i in range(len(notesUnique)):
		if i == index:
			new[i] = 1
		else:
			new[i] = 0
	return new

#find value of note
def noteValue(index):
	global notesUnique
	i = 0
	for x in index:
		if x == 1:
			noteVal = notesUnique[i]
		i += 1
	return noteVal

main()

