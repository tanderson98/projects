/*
 * Author: Tate Anderson
 * Date: 3/29/19
 * Object file for equipment
 */

package com.cinco;

public class Equipment extends Product{
	
	private double pricePerUnit;
	
	public Equipment(String productCode, char productType, String productName, double pricePerUnit) {
		super(productCode, productType, productName);
		this.pricePerUnit = pricePerUnit;
	}

	public double getFee() {
		return 0;
	}
	
	public double getProductSubtotal(String productString) {
		String tokens[] = productString.split(":");
		double numberOfUnits = Double.parseDouble(tokens[1]);
		return this.pricePerUnit*numberOfUnits;
	}
	
	public double getTax(String productString) {
		return 0.07*this.getProductSubtotal(productString);
	}
	
	public String getRate(String productString) {
		String tokens[] = productString.split(":");
		String roundedValue = String.format("%.2f", this.pricePerUnit);
		String rateString = "(" + tokens[1] + " units @ $" + roundedValue + "/unit)";
		return rateString;

	}
}
