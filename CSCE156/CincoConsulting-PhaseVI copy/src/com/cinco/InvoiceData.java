/* Author: Tate Anderson
 * Date: 04/11/19
 * This file contains the various methods to alter data in the database
 */

package com.cinco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * This is a collection of utility methods that define a general API for
 * interacting with the database supporting this application.
 *
 */
public class InvoiceData {

	/**
	 * Method that removes every person record from the database
	 */
	public static void removeAllPersons() {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			PreparedStatement removeEmails = conn.prepareStatement("delete from Email");
			removeEmails.execute();
			removeEmails.close();

			PreparedStatement removeInvoiceProduct = conn.prepareStatement("delete from InvoiceProduct");
			removeInvoiceProduct.execute();
			removeInvoiceProduct.close();

			PreparedStatement removeInvoices = conn.prepareStatement("delete from Invoice");
			removeInvoices.execute();
			removeInvoices.close();

			PreparedStatement removeCustomers = conn.prepareStatement("delete from Customer");
			removeCustomers.execute();
			removeCustomers.close();

			PreparedStatement removePersons = conn.prepareStatement("delete from Person");
			removePersons.execute();
			removePersons.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Removes the person record from the database corresponding to the provided
	 * <code>personCode</code>
	 * 
	 * @param personCode
	 */
	public static void removePerson(String personCode) {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			int personId = getPersonId(personCode, conn);

			// remove emails of given person
			PreparedStatement emailRemove = conn.prepareStatement("delete from Email where personId = ?");
			emailRemove.setInt(1, personId);
			emailRemove.execute();
			emailRemove.close();

			// get all invoice ids with given salesperson
			PreparedStatement getInvoiceIds = conn
					.prepareStatement("select invoiceId from Invoice where salespersonId = ?");
			getInvoiceIds.setString(1, personCode);
			ResultSet invoiceIdSet = getInvoiceIds.executeQuery();
			ArrayList<Integer> invoiceIdList = new ArrayList<>();
			while (invoiceIdSet.next()) {
				int invoiceId = invoiceIdSet.getInt("salespersonId");
				invoiceIdList.add(invoiceId);
			}
			getInvoiceIds.close();

			// loop through InvoiceProduct, delete all with given invoice ids
			for (int tempId : invoiceIdList) {
				PreparedStatement removeInvoiceProduct = conn
						.prepareStatement("delete from InvoiceProduct where invoiceId = ?");
				removeInvoiceProduct.setInt(1, tempId);
				removeInvoiceProduct.execute();
				removeInvoiceProduct.close();
			}

			// remove invoices with given salesperson id
			PreparedStatement removeInvoice = conn.prepareStatement("delete from Invoice where salespersonId = ?");
			removeInvoice.setInt(1, personId);
			removeInvoice.execute();
			removeInvoice.close();

			// remove customers with given primary contact ID
			PreparedStatement customerRemove = conn.prepareStatement("delete from Customer where primaryContactId = ?");
			customerRemove.setInt(1, personId);
			customerRemove.execute();
			customerRemove.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Method to add a person record to the database with the provided data.
	 * 
	 * @param personCode
	 * @param firstName
	 * @param lastName
	 * @param street
	 * @param city
	 * @param state
	 * @param zip
	 * @param country
	 */
	public static void addPerson(String personCode, String firstName, String lastName, String street, String city,
			String state, String zip, String country) {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		int addressId = getAddressId(street, city, zip, state, country, conn);

		try {
			PreparedStatement personInsert = conn.prepareStatement(
					"insert into Person (personCode, firstName, lastName, addressId) values (?,?,?,?)");
			personInsert.setString(1, personCode);
			personInsert.setString(2, firstName);
			personInsert.setString(3, lastName);
			personInsert.setInt(4, addressId);
			personInsert.execute();
			personInsert.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Adds an email record corresponding person record corresponding to the
	 * provided <code>personCode</code>
	 * 
	 * @param personCode
	 * @param email
	 */
	public static void addEmail(String personCode, String email) {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			
			int personId = getPersonId(personCode, conn);

			PreparedStatement addEmail = conn
					.prepareStatement("insert into Email (emailAddress, personId) values (?,?)");
			addEmail.setString(1, email);
			addEmail.setInt(2, personId);
			addEmail.execute();
			addEmail.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Method that removes every customer record from the database
	 */
	public static void removeAllCustomers() {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			PreparedStatement removeInvoiceProduct = conn.prepareStatement("delete from InvoiceProduct");
			removeInvoiceProduct.execute();
			removeInvoiceProduct.close();

			PreparedStatement removeInvoices = conn.prepareStatement("delete from Invoice");
			removeInvoices.execute();
			removeInvoices.close();

			PreparedStatement removeCustomers = conn.prepareStatement("delete from Customer");
			removeCustomers.execute();
			removeCustomers.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void addCustomer(String customerCode, String type, String primaryContactPersonCode, String name,
			String street, String city, String state, String zip, String country) {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		int primaryContactPersonId = getPersonId(primaryContactPersonCode, conn);
		int addressId = getAddressId(street, city, zip, state, country, conn);

		try {
			PreparedStatement addCustomer = conn.prepareStatement(
					"insert into Customer (customerCode, customerType, primaryContactId, customerName, addressId) values (?,?,?,?,?)");
			addCustomer.setString(1, customerCode);
			addCustomer.setString(2, type);
			addCustomer.setInt(3, primaryContactPersonId);
			addCustomer.setString(4, name);
			addCustomer.setInt(5, addressId);
			addCustomer.execute();
			addCustomer.close();

		} catch (SQLException e1) {
			e1.printStackTrace();
		}

	}

	/**
	 * Removes all product records from the database
	 */
	public static void removeAllProducts() {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			PreparedStatement removeInvoiceProduct = conn.prepareStatement("delete from InvoiceProduct");
			removeInvoiceProduct.execute();
			PreparedStatement removeProduct = conn.prepareStatement("delete from Product");
			removeProduct.execute();
			removeInvoiceProduct.close();
			removeProduct.close();

		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Removes a particular product record from the database corresponding to the
	 * provided <code>productCode</code>
	 * 
	 * @param assetCode
	 */
	public static void removeProduct(String productCode) {

		Integer productId;

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			productId = getProductId(productCode, conn);

			PreparedStatement removeInvoiceProduct = conn
					.prepareStatement("delete from InvoiceProduct where productId = ?");
			removeInvoiceProduct.setInt(1, productId);
			removeInvoiceProduct.execute();
			removeInvoiceProduct.close();

			PreparedStatement removeProduct = conn.prepareStatement("delete from Product where productId = ?");
			removeProduct.setInt(1, productId);
			removeProduct.execute();
			removeProduct.close();

		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Adds an equipment record to the database with the provided data.
	 */
	public static void addEquipment(String productCode, String name, Double pricePerUnit) {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {

			PreparedStatement addEquipment = conn.prepareStatement(
					"insert into Product (productCode, productName, pricePerUnit, productType) values (?,?,?,?)");
			addEquipment.setString(1, productCode);
			addEquipment.setString(2, name);
			addEquipment.setDouble(3, pricePerUnit);
			addEquipment.setString(4, "E");

			addEquipment.execute();
			addEquipment.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds an license record to the database with the provided data.
	 */
	public static void addLicense(String productCode, String name, double serviceFee, double annualFee) {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			PreparedStatement addLicense = conn.prepareStatement(
					"insert into Product (productCode, productName, serviceFee, annualLicenseFee, productType) values (?,?,?,?,?)");
			addLicense.setString(1, productCode);
			addLicense.setString(2, name);
			addLicense.setDouble(3, serviceFee);
			addLicense.setDouble(4, annualFee);
			addLicense.setString(5, "L");

			addLicense.execute();
			addLicense.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Adds an consultation record to the database with the provided data.
	 */
	public static void addConsultation(String productCode, String name, String consultantPersonCode, Double hourlyFee) {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			
			int consultantPersonId = getPersonId(consultantPersonCode, conn);
			PreparedStatement addConsultation = conn.prepareStatement(
					"insert into Product (productCode, productName, consultantPersonId, hourlyFee, productType) values (?,?,?,?,?)");
			addConsultation.setString(1, productCode);
			addConsultation.setString(2, name);
			addConsultation.setInt(3, consultantPersonId);
			addConsultation.setDouble(4, hourlyFee);
			addConsultation.setString(5, "C");

			addConsultation.execute();
			addConsultation.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Removes all invoice records from the database
	 */
	public static void removeAllInvoices() {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			PreparedStatement removeInvoiceProduct = conn.prepareStatement("delete from InvoiceProduct");
			removeInvoiceProduct.execute();
			removeInvoiceProduct.close();

			PreparedStatement removeInvoice = conn.prepareStatement("delete from Invoice");
			removeInvoice.execute();
			removeInvoice.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Removes the invoice record from the database corresponding to the provided
	 * <code>invoiceCode</code>
	 * 
	 * @param invoiceCode
	 */
	public static void removeInvoice(String invoiceCode) {

		Integer invoiceId;

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			invoiceId = getInvoiceId(invoiceCode, conn);

			PreparedStatement removeInvoiceProduct = conn
					.prepareStatement("delete from InvoiceProduct where invoiceId = ?");
			removeInvoiceProduct.setInt(1, invoiceId);
			removeInvoiceProduct.execute();
			removeInvoiceProduct.close();

			PreparedStatement removeInvoice = conn.prepareStatement("delete from Invoice where invoiceId = ?");
			removeInvoice.setInt(1, invoiceId);
			removeInvoice.execute();
			removeInvoice.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds an invoice record to the database with the given data.
	 */
	public static void addInvoice(String invoiceCode, String customerCode, String salesPersonCode) {
		Integer salespersonId = null, customerId = null;

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			PreparedStatement getCustomerId = conn
					.prepareStatement("select customerId from Customer where customerCode = ?");
			getCustomerId.setString(1, customerCode);
			ResultSet customerResult = getCustomerId.executeQuery();
			if (customerResult.next()) {
				customerId = customerResult.getInt("customerId");
			}
			getCustomerId.close();

			PreparedStatement getSalespersonId = conn
					.prepareStatement("select personId from Person where personCode = ?");
			getSalespersonId.setString(1, salesPersonCode);
			ResultSet salespersonResult = getSalespersonId.executeQuery();
			if (salespersonResult.next()) {
				salespersonId = salespersonResult.getInt("personId");
			}
			getSalespersonId.close();

			PreparedStatement addInvoice = conn
					.prepareStatement("insert into Invoice (invoiceCode, customerId, salespersonId) values (?,?,?)");
			addInvoice.setString(1, invoiceCode);
			addInvoice.setInt(2, customerId);
			addInvoice.setInt(3, salespersonId);
			addInvoice.execute();
			addInvoice.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Adds a particular equipment (corresponding to <code>productCode</code> to an
	 * invoice corresponding to the provided <code>invoiceCode</code> with the given
	 * number of units
	 */
	public static void addEquipmentToInvoice(String invoiceCode, String productCode, int numUnits) {
		Integer invoiceId = null, productId = null;

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			invoiceId = getInvoiceId(invoiceCode, conn);

			productId = getProductId(productCode, conn);

			PreparedStatement addEquipment = conn
					.prepareStatement("insert into InvoiceProduct (invoiceId, productId, totalUnits) values (?,?,?)");
			addEquipment.setInt(1, invoiceId);
			addEquipment.setInt(2, productId);
			addEquipment.setDouble(3, numUnits);
			addEquipment.execute();
			addEquipment.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Adds a particular equipment (corresponding to <code>productCode</code> to an
	 * invoice corresponding to the provided <code>invoiceCode</code> with the given
	 * begin/end dates
	 */
	public static void addLicenseToInvoice(String invoiceCode, String productCode, String startDate, String endDate) {

		Integer invoiceId = null, productId = null;

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			invoiceId = getInvoiceId(invoiceCode, conn);

			productId = getProductId(productCode, conn);

			PreparedStatement addEquipment = conn.prepareStatement(
					"insert into InvoiceProduct (invoiceId, productId, startDate, endDate) values (?,?,?,?)");
			addEquipment.setInt(1, invoiceId);
			addEquipment.setInt(2, productId);
			addEquipment.setString(3, startDate);
			addEquipment.setString(4, endDate);
			addEquipment.execute();
			addEquipment.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds a particular equipment (corresponding to <code>productCode</code> to an
	 * invoice corresponding to the provided <code>invoiceCode</code> with the given
	 * number of billable hours.
	 */
	public static void addConsultationToInvoice(String invoiceCode, String productCode, double numHours) {

		Integer invoiceId = null, productId = null;

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			invoiceId = getInvoiceId(invoiceCode, conn);

			productId = getProductId(productCode, conn);

			PreparedStatement addEquipment = conn
					.prepareStatement("insert into InvoiceProduct (invoiceId, productId, totalHours) values (?,?,?)");
			addEquipment.setInt(1, invoiceId);
			addEquipment.setInt(2, productId);
			addEquipment.setDouble(3, numHours);
			addEquipment.execute();
			addEquipment.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private static int getAddressId(String street, String city, String zip, String state, String country,
			Connection conn) {

		Integer stateId = null;

		try {
			PreparedStatement getStates = conn.prepareStatement("select * from State");
			ResultSet stateResults = getStates.executeQuery();
			while (stateResults.next()) {
				if (stateResults.getString("abbreviation").contentEquals(state)) {
					stateId = stateResults.getInt("stateId");
				}
			}
			getStates.close();

			if (stateId == null) {
				PreparedStatement stateInsert = conn.prepareStatement("insert into State (abbreviation) values (?)");
				stateInsert.setString(1, state);
				stateInsert.execute();
				stateInsert.close();

				PreparedStatement getState = conn.prepareStatement("select stateId from State where abbreviation = ?");
				getState.setString(1, state);
				ResultSet stateResult = getState.executeQuery();
				if (stateResult.next()) {
					stateId = stateResult.getInt("stateId");
				}
				getState.close();
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		Integer countryId = null;

		try {
			PreparedStatement getCountries = conn.prepareStatement("select * from Country");
			ResultSet countryResults = getCountries.executeQuery();
			while (countryResults.next()) {
				if (countryResults.getString("countryName").contentEquals(country)) {
					countryId = countryResults.getInt("countryId");
				}
			}
			getCountries.close();

			if (countryId == null) {
				PreparedStatement countryInsert = conn.prepareStatement("insert into Country (countryName) values (?)");
				countryInsert.setString(1, country);
				countryInsert.execute();
				countryInsert.close();

				PreparedStatement getCountry = conn
						.prepareStatement("select countryId from Country where countryName = ?");
				getCountry.setString(1, country);
				ResultSet countryResult = getCountry.executeQuery();
				if (countryResult.next()) {
					countryId = countryResult.getInt("countryId");
				}
				getCountry.close();
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		Integer addressId = null;

		try {
			PreparedStatement getAddresses = conn.prepareStatement("select * from Address");
			ResultSet addressResults = getAddresses.executeQuery();
			while (addressResults.next()) {
				if (addressResults.getString("street").contentEquals(street)
						&& addressResults.getString("city").contentEquals(city)
						&& addressResults.getString("zip").contentEquals(zip)
						&& addressResults.getInt("stateId") == stateId
						&& addressResults.getInt("countryId") == countryId) {
					addressId = addressResults.getInt("addressId");
				}
			}
			getAddresses.close();

			if (addressId == null) {
				PreparedStatement addressInsert = conn.prepareStatement(
						"insert into Address (street, city, zip, stateId, countryId) values (?,?,?,?,?)");
				addressInsert.setString(1, street);
				addressInsert.setString(2, city);
				addressInsert.setString(3, zip);
				addressInsert.setInt(4, stateId);
				addressInsert.setInt(5, countryId);

				addressInsert.execute();
				addressInsert.close();

				PreparedStatement getAddress = conn.prepareStatement("select addressId from Address where street = ?");
				getAddress.setString(1, street);
				ResultSet addressResult = getAddress.executeQuery();
				if (addressResult.next()) {
					addressId = addressResult.getInt("addressId");
				}
				getAddress.close();
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		return addressId;

	}

	private static int getInvoiceId(String invoiceCode, Connection conn) {

		Integer invoiceId = null;
		try {
			PreparedStatement getInvoiceId = conn
					.prepareStatement("select invoiceId from Invoice where invoiceCode = ?");
			getInvoiceId.setString(1, invoiceCode);
			ResultSet invoiceResult = getInvoiceId.executeQuery();
			if (invoiceResult.next()) {
				invoiceId = invoiceResult.getInt("invoiceId");
			}
			getInvoiceId.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return invoiceId;
	}

	private static int getProductId(String productCode, Connection conn) {

		Integer productId = null;
		try {
			PreparedStatement getProductId = conn
					.prepareStatement("select productId from Product where productCode = ?");
			getProductId.setString(1, productCode);
			ResultSet productResult = getProductId.executeQuery();
			if (productResult.next()) {
				productId = productResult.getInt("productId");
			}
			getProductId.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return productId;

	}

	private static int getPersonId(String personCode, Connection conn) {
		Integer personId = null;

		try {
			PreparedStatement getPersonId = conn.prepareStatement("select personId from Person where personCode = ?");
			getPersonId.setString(1, personCode);
			ResultSet personResult = getPersonId.executeQuery();
			if (personResult.next()) {
				personId = personResult.getInt("personId");
			}
			getPersonId.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return personId;

	}

}
