/*
 * Author: Tate Anderson
 * Date: 4/18/19
 * This file is the sorted list ADT for the invoice system
 */

package com.cinco;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class SortedList<T> implements Iterable<T> {

	private T arr[];
	public int size;
	private Comparator<T> comp;

	@SuppressWarnings("unchecked")
	public SortedList(Comparator<T> comparator) {
		this.arr = (T[]) new Object[10];
		this.size = 0;
		this.comp = comparator;
	}

	// method when adding new element
	public void add(T item) {

		// if inserting first element
		if (this.size == 0) {
			arr[0] = item;
			size++;
			return;
		}

		// otherwise insert at correct index
		int i = 0;
		for (i = 0; i < size; i++) {
			if (this.comp.compare(arr[i], item) > 0) {
				this.insertElementAtIndex(item, i);
				break;
			}
		}

		// if element is greater than all current
		if (i == size) {
			this.addElementToEnd(item);
		}
	}

	public void addElementToEnd(T item) {
		this.insertElementAtIndex(item, this.size);
	}

	// method used to insert at given index
	public void insertElementAtIndex(T item, int index) {
		if (index < 0 || index > this.size) {
			throw new IllegalArgumentException("out of bounds");
		}
		// copy and extend size
		if (this.size == this.arr.length) {
			this.arr = Arrays.copyOf(this.arr, this.size + 1);
		}

		// shift down
		for (int i = this.size - 1; i >= index; i--) {
			this.arr[i + 1] = this.arr[i];
		}
		this.arr[index] = item;
		this.size++;
	}

	// retrieve element at index
	public T getElementAtIndex(int index) {
		if (index < 0 || index >= this.size) {
			throw new IllegalArgumentException("out of bounds");
		}
		return this.arr[index];
	}

	// remove element
	public T removeElementAtIndex(int index) {
		if (index < 0 || index >= this.size) {
			throw new IllegalArgumentException("out of bounds");
		}
		T item = this.arr[index];
		// shift down
		for (int i = index; i < this.size - 1; i++) {
			this.arr[i] = this.arr[i + 1];
		}
		this.size--;
		return item;
	}

	// iterator to allow list to be iterated over
	public Iterator<T> iterator() {
		return new SortedListIterator();
	}

	private class SortedListIterator implements Iterator<T> {
		private int index = 0;

		public boolean hasNext() {
			return (index < size);
		}

		public T next() {
			if (hasNext()) {
				return arr[index++];
			} else {
				throw new NoSuchElementException();
			}
		}
	}

}