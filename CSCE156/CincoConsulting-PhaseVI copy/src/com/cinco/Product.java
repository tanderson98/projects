/*
 * Author: Tate Anderson
 * Date: 3/29/19
 * Abstract object file for products
 */

package com.cinco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public abstract class Product {
	
	private String productCode;
	private char productType;
	private String productName;
	
	public Product(String productCode, char productType, String productName) {
		this.productCode = productCode.trim();
		this.productType = productType;
		this.productName = productName.trim();
	}
	

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public char getProductType() {
		return productType;
	}

	public void setProductType(char productType) {
		this.productType = productType;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public abstract double getFee();
	
	public abstract double getProductSubtotal(String productString);
	
	public abstract double getTax(String productString);
	
	public abstract String getRate(String productString);
	
	//get products from database, put into ArrayList
	public static ArrayList<Product> getProductArray(Connection conn) {
		
		ArrayList<Product> productArray = new ArrayList<>();
		
		try {
			PreparedStatement ps = conn.prepareStatement("select * from Product");
			
			ResultSet productResultSet = ps.executeQuery();
			
			while(productResultSet.next()) {
				String productCode = productResultSet.getString("productCode");
				String productTypeString = productResultSet.getString("productType");
				char productType = productTypeString.charAt(0);
				String productName = productResultSet.getString("productName");
				if(productType == 'E') {
					double pricePerUnit = productResultSet.getDouble("pricePerUnit");
					Equipment equipment = new Equipment(productCode, productType, productName, pricePerUnit);
					productArray.add(equipment);
				}
				else if(productType == 'L') {
					double serviceFee = productResultSet.getDouble("serviceFee");
					double annualLicenseFee = productResultSet.getDouble("annualLicenseFee");
					License license = new License(productCode, productType, productName, serviceFee, annualLicenseFee);
					productArray.add(license);
				}
				else if(productType == 'C') {
					double hourlyFee = productResultSet.getDouble("hourlyFee");
					String consultantPersonId = productResultSet.getString("consultantPersonId");
					Consultation consultation = new Consultation(productCode, productType, productName, consultantPersonId, hourlyFee);
					productArray.add(consultation);
				}
			}
			
			ps.close();
			return productArray;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
		
	}
}


