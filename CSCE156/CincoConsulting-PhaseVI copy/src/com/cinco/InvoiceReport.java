/*
 * Author: Tate Anderson
 * Date: 3/29/19
 * this is the main driver class that calls the various method to retrieve and print the info from the database
 */

package com.cinco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

public class InvoiceReport {

	public static void main(String args[]) {
	
		Connection conn = null;
		try {
		conn = DriverManager.getConnection(DatabaseInfo.url, DatabaseInfo.username, DatabaseInfo.password);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
				
		ArrayList<Person> personList = Person.getPersonArray(conn);
		ArrayList<Customer> customerList = Customer.getCustomerArray(conn, personList);
		ArrayList<Product> productMasterList = Product.getProductArray(conn);
		
		SortedList<Invoice> invoiceListName = Invoice.getInvoiceList(conn, customerList, productMasterList, Invoice.nameCompare);
		SortedList<Invoice> invoiceListTotal = Invoice.getInvoiceList(conn, customerList, productMasterList, Invoice.totalCompare);
		SortedList<Invoice> invoiceListType = Invoice.getInvoiceList(conn, customerList, productMasterList, Invoice.typeCompare);

		
		try {
			if(conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		System.out.println("BY CUSTOMER NAME");
		Invoice.printInvoiceReport(customerList, personList, productMasterList, invoiceListName);
		System.out.println("BY INVOICE TOTAL");
		Invoice.printInvoiceReport(customerList, personList, productMasterList, invoiceListTotal);
		System.out.println("BY CUSTOMER TYPE - SALESPERSON");
		Invoice.printInvoiceReport(customerList, personList, productMasterList, invoiceListType);

	}
}
