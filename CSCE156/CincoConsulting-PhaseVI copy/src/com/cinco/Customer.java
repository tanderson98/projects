/*
 * Author: Tate Anderson
 * Date: 3/29/19
 * Object file for customers
 */

package com.cinco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Customer {

	private String customerCode;
	private String type;
	private String primaryContactCode;
	private String customerName;
	private Address address;
	
	public Customer(String customerCode, String type, String primaryContactCode, String customerName, Address address) {		
		this.customerCode = customerCode;
		this.type = type;
		this.primaryContactCode = primaryContactCode;
		this.customerName = customerName;
		this.address = address;
		
	}
	
	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPrimaryContactCode() {
		return primaryContactCode;
	}

	public void setPrimaryContactCode(String primaryContactCode) {
		this.primaryContactCode = primaryContactCode;
	}

	public String getName() {
		return customerName;
	}

	public void setName(String customerName) {
		this.customerName = customerName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	

	//get customers from database, put into ArrayList
	public static ArrayList<Customer> getCustomerArray(Connection conn, ArrayList<Person> personList) {
		
		ArrayList<Customer> customerArray = new ArrayList<>();
		
		try {
			PreparedStatement ps = conn.prepareStatement("select c.customerCode, c.customerType, p.personCode, c.customerName, a.*, s.abbreviation, o.countryName "
													   + "from Customer c join Person p on c.primaryContactId = p.personId "
													   + "join Address a on c.addressId = a.addressId "
													   + "join State s on a.stateId = s.stateId "
													   + "join Country o on a.countryId = o.countryId");
			ResultSet customerResultSet = ps.executeQuery();
			
			while(customerResultSet.next()) {
				String customerCode = customerResultSet.getString("customerCode");
				String customerTypeString = customerResultSet.getString("customerType");
				String primaryContactCode = customerResultSet.getString("personCode");
				String customerName = customerResultSet.getString("customerName");
				String state = customerResultSet.getString("abbreviation");
				String country = customerResultSet.getString("countryName");
				String street = customerResultSet.getString("street");
				String city = customerResultSet.getString("city");
				String zip = customerResultSet.getString("zip");
				
				
				Address address = new Address(street, city, state, zip, country);
				Customer customer = new Customer(customerCode, customerTypeString, primaryContactCode, customerName, address);
				customerArray.add(customer);
				
			}
			
			ps.close();
			return customerArray;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

		
	}
}
