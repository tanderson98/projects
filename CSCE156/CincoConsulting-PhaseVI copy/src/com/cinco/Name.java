/*
 * Author: Tate Anderson
 * Date: 3/29/19
 * Object file for names
 */

package com.cinco;

public class Name {
	
	private String firstName;
	private String lastName;
	
	public Name(String lastName, String firstName) {
		this.firstName = firstName.trim();
		this.lastName = lastName.trim();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
