/*
 * Author: Tate Anderson
 * Date: 3/29/19
 * Object file for invoices
 */

package com.cinco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;

public class Invoice {

	private String invoiceCode;
	private String salespersonCode;
	private String customerCode;
	private Customer customer;
	private ArrayList<String> productList;
	private double total;
	private Name salespersonName;

	public String getInvoiceCode() {
		return invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public String getSalespersonCode() {
		return salespersonCode;
	}

	public void setSalespersonCode(String salespersonCode) {
		this.salespersonCode = salespersonCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public ArrayList<String> getProductList() {
		return productList;
	}

	public void setProductList(ArrayList<String> productList) {
		this.productList = productList;
	}

	public Customer getInvoiceCustomer() {
		return customer;
	}

	public double getInvoiceTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
	
	public Name getSalespersonName() {
		return salespersonName;
	}

	public Invoice(String invoiceCode, String salespersonCode, String customerCode, Customer customer,
			ArrayList<String> productList, double total, Name salespersonName) {
		super();
		this.invoiceCode = invoiceCode;
		this.salespersonCode = salespersonCode;
		this.customerCode = customerCode;
		this.customer = customer;
		this.productList = productList;
		this.total = total;
		this.salespersonName = salespersonName;
	}

	// get invoice data from database, convert to SortedList
	public static SortedList<Invoice> getInvoiceList(Connection conn, ArrayList<Customer> customerList,
			ArrayList<Product> productMasterList, Comparator<Invoice> comp) {

		SortedList<Invoice> invoiceList = new SortedList<>(comp);

		try {
			PreparedStatement ps = conn.prepareStatement("select i.*, p.*, c.customerCode from Invoice i "
					+ "join Customer c on i.customerId = c.customerId "
					+ "join Person p on i.salespersonId = p.personId");
			ResultSet invoiceResultSet = ps.executeQuery();

			double total;
			while (invoiceResultSet.next()) {
				String invoiceCode = invoiceResultSet.getString("invoiceCode");
				String salespersonCode = invoiceResultSet.getString("personCode");
				String customerCode = invoiceResultSet.getString("customerCode");
				ArrayList<String> productArray = new ArrayList<>();
				Customer customer = Invoice.getCustomer(customerCode, customerList);
				String salespersonLastName = invoiceResultSet.getString("lastName");
				String salespersonFirstName = invoiceResultSet.getString("firstName");
				Name salespersonFullName = new Name(salespersonLastName, salespersonFirstName);

				total = 0;

				Invoice invoice = new Invoice(invoiceCode, salespersonCode, customerCode, customer, productArray,
						total, salespersonFullName);

				invoiceList.add(invoice);
			}

			ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		// get product list in string format
		try {
			PreparedStatement ps = conn.prepareStatement("select ip.*, i.invoiceCode, p.* from InvoiceProduct ip "
					+ "join Invoice i on ip.invoiceId = i.invoiceId " + "join Product p on p.productId = ip.productId");
			ResultSet invoiceProductResultSet = ps.executeQuery();

			while (invoiceProductResultSet.next()) {
				String productCode = invoiceProductResultSet.getString("productCode");
				String productType = invoiceProductResultSet.getString("productType");
				String invoiceCode = invoiceProductResultSet.getString("invoiceCode");

				String productString = productCode;

				if (productType.contentEquals("E")) {
					int totalUnits = invoiceProductResultSet.getInt("totalUnits");
					productString += ":" + totalUnits;

				} else if (productType.contentEquals("C")) {
					double totalHours = invoiceProductResultSet.getDouble("totalHours");
					productString += ":" + totalHours;
				} else if (productType.contentEquals("L")) {
					String startDate = invoiceProductResultSet.getString("startDate");
					String endDate = invoiceProductResultSet.getString("endDate");
					productString += ":" + startDate;
					productString += ":" + endDate;
				}

				for (Invoice invoice : invoiceList) {
					if (invoice != null) {
						if (invoice.getInvoiceCode().contentEquals(invoiceCode)) {
							invoice.getProductList().add(productString);
						}
					}
				}

			}

			ps.close();

			// for calculating total
			SortedList<Invoice> sortedInvoiceList = new SortedList<Invoice>(comp);
			for (Invoice tempInvoice : invoiceList) {
				tempInvoice.setTotal(tempInvoice.getTotal(productMasterList, customerList));
				sortedInvoiceList.add(tempInvoice);
			}

			return sortedInvoiceList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

	}

	// method to get customer from list of customers
	public static Customer getCustomer(String customerCode, ArrayList<Customer> customerList) {
		Customer customer = null;
		for (Customer tempCustomer : customerList) {
			if (tempCustomer.getCustomerCode().equals(customerCode)) {
				customer = tempCustomer;
			}
		}
		return customer;
	}

	// method to get salesperson from list of people
	public Person getSalesperson(String salespersonCode, ArrayList<Person> personList) {
		Person salesperson = null;
		for (Person tempPerson : personList) {
			if (tempPerson.getPersonCode().equals(salespersonCode)) {
				salesperson = tempPerson;
			}
		}
		return salesperson;
	}

	// method to get product from product list
	public Product getProduct(String productCode, ArrayList<Product> productList) {
		Product product = null;
		for (Product tempProduct : productList) {
			if (tempProduct.getProductCode().equals(productCode)) {
				product = tempProduct;
			}
		}
		return product;
	}

	// method to get fees from invoice
	public double getFees(ArrayList<Customer> customerList, ArrayList<Product> productMasterList) {
		double fees = 0;
		if (getCustomer(this.getCustomerCode(), customerList).getType().contentEquals("G")) {
			fees = 125;
		}
		for (String tempProductString : this.productList) {
			String productTokens[] = tempProductString.split(":");
			Product newProduct = getProduct(productTokens[0], productMasterList);
			fees += newProduct.getFee();
		}
		return fees;
	}

	// method to get subtotal of invoice
	public double getSubtotal(ArrayList<Product> productMasterList) {
		double subtotal = 0;
		for (String tempProductString : this.getProductList()) {
			String productTokens[] = tempProductString.split(":");
			Product tempProduct = getProduct(productTokens[0], productMasterList);
			subtotal += tempProduct.getProductSubtotal(tempProductString);
		}
		return subtotal;

	}

	// method to get total taxes of invoice
	public double getTaxes(ArrayList<Customer> customerList, ArrayList<Product> productMasterList) {
		double tax = 0;
		if (getCustomer(this.getCustomerCode(), customerList).getType().contentEquals("G")) {
			tax = 0;
		} else {
			for (String tempProductString : this.getProductList()) {
				String productTokens[] = tempProductString.split(":");
				Product tempProduct = getProduct(productTokens[0], productMasterList);
				tax += tempProduct.getTax(tempProductString);
			}
		}
		return tax;
	}

	// method to get total price of invoice
	public double getTotal(ArrayList<Product> productMasterList, ArrayList<Customer> customerList) {
		return this.getSubtotal(productMasterList) + this.getFees(customerList, productMasterList)
				+ this.getTaxes(customerList, productMasterList);
	}

	// method to print invoice list
	public static void printInvoiceReport(ArrayList<Customer> customerList, ArrayList<Person> personList,
			ArrayList<Product> productMasterList, SortedList<Invoice> invoiceList) {
		System.out.println("Executive Summary Report");
		System.out.println("=========================");
		System.out.printf("%-10s %-30s %-22s %-16s %-13s %-13s %-13s\n", "Invoice", "Customer", "Salesperson",
				"Subtotal", "Fees", "Taxes", "Total");

		double finalSubtotal = 0;
		double finalFees = 0;
		double finalTaxes = 0;
		double finalTotal = 0;

		for (Invoice tempInvoice : invoiceList) {
			System.out.printf("%-10s %-30s %-10s, %-10s $%-15.2f $%-12.2f $%-12.2f $%-12.2f\n",
					tempInvoice.getInvoiceCode(),
					Invoice.getCustomer(tempInvoice.getCustomerCode(), customerList).getName(),
					tempInvoice.getSalesperson(tempInvoice.getSalespersonCode(), personList).getName().getLastName(),
					tempInvoice.getSalesperson(tempInvoice.getSalespersonCode(), personList).getName().getFirstName(),
					tempInvoice.getSubtotal(productMasterList), tempInvoice.getFees(customerList, productMasterList),
					tempInvoice.getTaxes(customerList, productMasterList),
					tempInvoice.getTotal(productMasterList, customerList));

			finalSubtotal += tempInvoice.getSubtotal(productMasterList);
			finalFees += tempInvoice.getFees(customerList, productMasterList);
			finalTaxes += tempInvoice.getTaxes(customerList, productMasterList);
			finalTotal += tempInvoice.getTotal(productMasterList, customerList);
		}

		System.out.println(
				"=============================================================================================================================");
		System.out.printf("%-64s $%-15.2f $%-12.2f $%-12.2f $%-12.2f\n\n\n\n", "TOTALS", finalSubtotal, finalFees,
				finalTaxes, finalTotal);

	}

	// comparing by customer name
	public static Comparator<Invoice> nameCompare = new Comparator<Invoice>() {

		public int compare(Invoice i1, Invoice i2) {

			return i1.getInvoiceCustomer().getName().compareTo(i2.getInvoiceCustomer().getName());
		}
	};

	// comparing by customer type, then by salesperson name
	public static Comparator<Invoice> typeCompare = new Comparator<Invoice>() {

		public int compare(Invoice i1, Invoice i2) {

			//if types are same, return last name compare, if those are same, return first name compare
			int typeResult = i1.getInvoiceCustomer().getType().compareTo(i2.getInvoiceCustomer().getType());
			int lastNameResult = i1.getSalespersonName().getLastName().compareTo(i2.getSalespersonName().getLastName());
			if (typeResult == 0) {
				if(lastNameResult == 0) {
					return i1.getSalespersonName().getFirstName().compareTo(i2.getSalespersonName().getFirstName());
				} else {
					return lastNameResult;
				}
			} else {
				return typeResult;
			}
		}
	};

	// comparing by invoice total
	public static Comparator<Invoice> totalCompare = new Comparator<Invoice>() {

		public int compare(Invoice i1, Invoice i2) {

			return -Double.compare(i1.getInvoiceTotal(), i2.getInvoiceTotal());
		}
	};

}
