/*
 * Author: Tate Anderson
 * Date: 3/29/19
 * Object file for persons
 */

package com.cinco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Person {
	
	private String personCode;
	private Name name;
	private Address address;
	
	public Person(String personCode, Name name, Address address) {
		this.personCode = personCode;
		this.name = name;
		this.address = address;
	}
	
	
	public String getPersonCode() {
		return personCode;
	}

	public void setPersonCode(String personCode) {
		this.personCode = personCode;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	
	public static ArrayList<Person> getPersonArray(Connection conn) {
		
		ArrayList<Person> personArray = new ArrayList<>();
		
		try {
			PreparedStatement ps = conn.prepareStatement("select p.*, s.*, c.*, a.* from Person p "
													   + "join Address a on p.addressId = a.addressId "
													   + "join State s on a.stateId = s.stateId "
													   + "join Country c on a.countryId = c.countryId");
			
			ResultSet personResultSet = ps.executeQuery();
			while(personResultSet.next()) {
				String personCode = personResultSet.getString("personCode");
				String firstName = personResultSet.getString("firstName");
				String lastName = personResultSet.getString("lastName");
				Name name = new Name(lastName, firstName);
				String state = personResultSet.getString("abbreviation");
				String country = personResultSet.getString("countryName");
				String street = personResultSet.getString("street");
				String city = personResultSet.getString("city");
				String zip = personResultSet.getString("zip");				
				Address address = new Address(street, city, state, zip, country);				
				
				Person person = new Person(personCode, name, address);
				personArray.add(person);
				
			}
			
			ps.close();
			return personArray;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
		
	}
	
	public static Person getPerson(String personCode, ArrayList<Person> personList) {
		
		Person tempPerson = new Person(null, null, null);
		
		for(Person person : personList) {
			if(person.getPersonCode().contentEquals(personCode)) {
				return person;
			}
		}
		return tempPerson;
	}

}
