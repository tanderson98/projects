/*
 * Author: Tate Anderson
 * Date: 3/29/19
 * Object file for licenses
 */

package com.cinco;


import java.util.Calendar;
import java.util.GregorianCalendar;

public class License extends Product{
	
	private double serviceFee;
	private double annualLicenseFee;
	
	public License(String productCode, char productType, String productName, double serviceFee, double annualLicenseFee) {
		super(productCode, productType, productName);
		this.serviceFee = serviceFee;
		this.annualLicenseFee = annualLicenseFee;
	}
	
	public double getServiceFee() {
		return serviceFee;
	}
	
	public double getAnnualLicenseFee() {
		return annualLicenseFee;
	}
	
	public double getFee() {
		return this.serviceFee;
	}
	
	public double getProductSubtotal(String productString) {
		String tokens[] = productString.split(":");
		String startDateArray[] = tokens[1].split("-");
		String endDateArray[] = tokens[2].split("-");
		Calendar startDate = new GregorianCalendar();
		startDate.set(Integer.parseInt(startDateArray[0]), Integer.parseInt(startDateArray[1]), Integer.parseInt(startDateArray[2]));
		Calendar endDate = new GregorianCalendar();
		endDate.set(Integer.parseInt(endDateArray[0]), Integer.parseInt(endDateArray[1]), Integer.parseInt(endDateArray[2]));
		double daysBetween = (endDate.getTimeInMillis() - startDate.getTimeInMillis()) / (1000 * 60 * 60 * 24);
		return (daysBetween*this.getAnnualLicenseFee()) / 365;

		
	}
	
	public double getTax(String productString) {
		return 0.0425*this.getProductSubtotal(productString);
	}
	
	public String getRate(String productString) {
		String tokens[] = productString.split(":");
		String startDateArray[] = tokens[1].split("-");
		String endDateArray[] = tokens[2].split("-");
		Calendar startDate = new GregorianCalendar();
		startDate.set(Integer.parseInt(startDateArray[0]), Integer.parseInt(startDateArray[1]), Integer.parseInt(startDateArray[2]));
		Calendar endDate = new GregorianCalendar();
		endDate.set(Integer.parseInt(endDateArray[0]), Integer.parseInt(endDateArray[1]), Integer.parseInt(endDateArray[2]));
		int daysBetween = (int) ((endDate.getTimeInMillis() - startDate.getTimeInMillis()) / (1000 * 60 * 60 * 24));		
		String roundedValue = String.format("%.2f", this.annualLicenseFee);
		String rateString = "(" + daysBetween + " days @ $" + roundedValue + "/yr;" + tokens[1] + ", " +  tokens[2] + ")";
		return rateString;
	}
}
