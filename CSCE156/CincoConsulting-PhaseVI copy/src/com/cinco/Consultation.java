/*
 * Author: Tate Anderson
 * Date: 3/29/19
 * Object file for consultations
 */

package com.cinco;

public class Consultation extends Product{
	
	private String consultantPersonId;
	private double hourlyFee;
	
	public Consultation(String productCode, char productType, String productName, String consultantPersonId, double hourlyFee) {
		super(productCode, productType, productName);
		this.consultantPersonId = consultantPersonId;
		this.hourlyFee = hourlyFee;
	}
	
	public String getConsultantPersonId() {
		return consultantPersonId;
	}

	public void setConsultantPersonId(String consultantPersonId) {
		this.consultantPersonId = consultantPersonId;
	}

	public double getHourlyFee() {
		return hourlyFee;
	}

	public void setHourlyFee(double hourlyFee) {
		this.hourlyFee = hourlyFee;
	}

	public double getFee() {
		return 150;
	}
	
	public double getProductSubtotal(String productString) {
		String tokens[] = productString.split(":");
		double numberOfHours = Double.parseDouble(tokens[1]);
		return this.hourlyFee*numberOfHours;
	}
	
	public double getTax(String productString) {
		return 0.0425*this.getProductSubtotal(productString);
	}
	
	public String getRate(String productString) {
		String tokens[] = productString.split(":");
		String roundedValue = String.format("%.2f", this.hourlyFee);
		String rateString = "(" + tokens[1] + " hours @ $" + roundedValue + "/hr)";
		return rateString;
	}
}
